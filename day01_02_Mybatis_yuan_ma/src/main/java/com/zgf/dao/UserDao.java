package com.zgf.dao;

import com.zgf.domain.User;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.util.ArrayList;
import java.util.List;

public interface UserDao {


    List<User> findAll();


}
