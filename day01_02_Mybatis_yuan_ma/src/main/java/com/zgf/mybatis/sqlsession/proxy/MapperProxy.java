package com.zgf.mybatis.sqlsession.proxy;

import com.zgf.mybatis.cfg.Mapper;
import com.zgf.mybatis.utils.Executor;

import java.awt.image.Kernel;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.Map;

public class MapperProxy implements InvocationHandler {

    private Map<String, Mapper> mappers;
    private Connection connection;

    public MapperProxy(Map<String, Mapper> mappers, Connection connection) {
        this.mappers = mappers;
        this.connection=connection;
    }

    /**
 * 增强方法，增强selectList方法*/
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName =method.getName();
        String ClassName =method.getDeclaringClass().getName();
        String key=ClassName+"."+methodName;
        Mapper mapper=mappers.get(key);
        if (mapper==null){
            throw  new RuntimeException("传入的参数有错误！");
        }
        return new Executor().selectList(mapper,connection);

    }
}
