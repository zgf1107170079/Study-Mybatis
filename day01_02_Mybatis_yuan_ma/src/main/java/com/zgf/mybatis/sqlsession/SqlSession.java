package com.zgf.mybatis.sqlsession;
/**
 * Mybatis中和数据库交互的核心类
 * 可以创建dao接口代理对象
 * */
public interface SqlSession {
    /**
     * 根据参数创建一个代理对象
     * */
    <T> T getMapper(Class<T> daoInterfaceClass);

    void close();

}
