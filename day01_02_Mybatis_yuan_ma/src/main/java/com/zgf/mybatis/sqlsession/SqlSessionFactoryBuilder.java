package com.zgf.mybatis.sqlsession;

import com.zgf.mybatis.cfg.Configuration;
import com.zgf.mybatis.sqlsession.impl.SqlSessionFactoryimpl;
import com.zgf.mybatis.utils.XMLConfigBuilder;

import java.io.InputStream;

/**
 * 用于创建一个SqlSessionFactory对象
 * */
public class SqlSessionFactoryBuilder {
/**
 * 根据输入流构建一个SqlSessionFactory工厂
* */
    public  SqlSessionFactory build(InputStream config){
        Configuration cfg= XMLConfigBuilder.loadConfiguration(config);
             return new SqlSessionFactoryimpl(cfg);
    }
}
