package com.zgf.mybatis.sqlsession.impl;

import com.zgf.mybatis.cfg.Configuration;
import com.zgf.mybatis.sqlsession.SqlSession;
import com.zgf.mybatis.sqlsession.SqlSessionFactory;

public class SqlSessionFactoryimpl implements SqlSessionFactory {
    private Configuration cfg;

    public SqlSessionFactoryimpl(Configuration cfg) {
        this.cfg = cfg;
    }

    /**
     * 用于创建一个新的操作数据库对象
     * */
    @Override
    public SqlSession openSession() {
        return new SqlSessionImpl(cfg);
    }
}
