package com.zgf.mybatis.sqlsession;

public interface SqlSessionFactory {


    /**
     * 用户打开一个新的sqlsession对象*/
    SqlSession openSession();
}

