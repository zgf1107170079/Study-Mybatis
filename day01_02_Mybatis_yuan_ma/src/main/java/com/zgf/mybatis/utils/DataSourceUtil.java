package com.zgf.mybatis.utils;

import com.zgf.mybatis.cfg.Configuration;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;

/**
 * 创建数据源
 * */
public class DataSourceUtil {
/**
 * 获取一个链接 连接数据库*/
    public static Connection getConnection(Configuration cfg){

        try {
            Class.forName(cfg.getDriver());
        return DriverManager.getConnection(cfg.getUrl(),cfg.getUsername(),cfg.getPassword());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
