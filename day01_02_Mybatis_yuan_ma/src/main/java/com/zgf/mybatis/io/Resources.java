package com.zgf.mybatis.io;

import java.io.InputStream;
/**
 * 根据类加载器，读取配置文件的类。
 *
 * */
public class Resources {

    /**
     * 根据传入的参数，获取一个输入流。
     * */
    public static InputStream getResourceAsStream(String filePath){
        return Resources.class.getClassLoader().getResourceAsStream(filePath);
    }
}
