package com.zgf.Test;

import com.zgf.dao.UserDao;
import com.zgf.domain.User;
import com.zgf.mybatis.io.Resources;
import com.zgf.mybatis.sqlsession.SqlSession;
import com.zgf.mybatis.sqlsession.SqlSessionFactory;
import com.zgf.mybatis.sqlsession.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MybatisTest {
    public static void main(String[] args) throws IOException {

        InputStream in = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(in);
        SqlSession session = sqlSessionFactory.openSession();
        UserDao userDao=session.getMapper(UserDao.class);
        List<User> users=  userDao.findAll();
        for (User user : users) {
            System.out.println(user);
        }
        session.close();
        in.close();
        }

}
