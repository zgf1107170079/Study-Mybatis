package com.zgf.dao;

import com.zgf.domain.QueryVo;
import com.zgf.domain.User;

import java.util.List;

public interface UserDao {

    List<User> listAll();

    void insertUser(User user);


    void updateUser(User user);

    void deleteUser(int id);

    List<User> findByName(String username);

    int UserTotal();

    List<User> findById(int id);

    List<User> findByVo(QueryVo queryVo);
}
