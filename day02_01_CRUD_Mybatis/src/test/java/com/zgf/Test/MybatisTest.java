package com.zgf.Test;

import com.zgf.dao.UserDao;
import com.zgf.domain.QueryVo;
import com.zgf.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

public class MybatisTest {

    private InputStream in;
    private SqlSession session;
    private UserDao userDao;
    @Before
    public void init() throws IOException {
        in = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(in);
        session = sqlSessionFactory.openSession();
        userDao=session.getMapper(UserDao.class);
    }
    @After
    public void destroy() throws IOException {
        session.commit();
        session.close();
        in.close();
    }
/**
 * 遍历
 * */
    @Test
    public void listAll(){
        List<User> users=  userDao.listAll();
        for (User user : users) {
            System.out.println(user);
        }
    }
/**增加*/
    @Test
    public void insertUser(){
        User user=new User();
        user.setUsername("郑国锋");
        user.setAddress("山东青岛");
        user.setSex("女");
        user.setBirthday(new Date());
        userDao.insertUser(user);
    }
/**
 * 删除
 * */
    @Test
    public void deleteUser(){
        userDao.deleteUser(41);
    }


    /**
     * 修改*/
    @Test
    public void updateUser(){
        User user=new User();
        user.setId(4);
        user.setUsername("曹操");
        user.setAddress("山东青岛");
        user.setSex("男");
        user.setBirthday(new Date());
        userDao.updateUser(user);
    }


/**根据用户名查询*/
    @Test
    public void findByName(){
        List<User> users= userDao.findByName("郑");
        for (User user : users) {
            System.out.println(user);
        }
    }


/**查询数据库数量*/
    @Test
    public void UserTotal(){
        int num=userDao.UserTotal();
        System.out.println(num);
    }


    /**
     * 根据ID查询
     * */
    @Test
    public void findById(){
        List<User> list = userDao.findById(1);
        System.out.println(list);
    }

    /**
     * 根据QueryVo查询
     * */
    @Test
    public void findByVo(){
        QueryVo queryVo=new QueryVo();
        User user=new User();
        user.setUsername("郑");
        queryVo.setUser(user);
       List<User> list= userDao.findByVo(queryVo);

        for (User user1 : list) {
            System.out.println(user1);
        }
    }
}
