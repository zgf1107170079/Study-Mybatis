package com.zgf.dao;

import com.zgf.domain.Account;

import java.util.List;

/**
 * @author zgf
 * @date 2019/9/4 20:24
 */
public interface AccountDao {

    List<Account> listAllAccount();

    Account findById(int i);
}
