package com.zgf.Test;

import com.zgf.dao.AccountDao;
import com.zgf.dao.UserDao;
import com.zgf.domain.Account;
import com.zgf.domain.AccountUser;
import com.zgf.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class AccountTest {

    private InputStream in;
    private SqlSession session;
    private UserDao userDao;

    private AccountDao accountDao;
    @Before
    public void init() throws IOException {
        in = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(in);
        session = sqlSessionFactory.openSession();
        userDao=session.getMapper(UserDao.class);
        accountDao=session.getMapper(AccountDao.class);
    }
    @After
    public void destroy() throws IOException {
        session.commit();
        session.close();
        in.close();
    }
/**
 * 遍历
 * */
   @Test
    public void listAll(){
        List<User> users=  userDao.listAll();
        for (User user : users) {
            System.out.println(user);
        }
    }

    /**
     * 遍历ACCOUNT*/
    @Test
    public void listAllAccount(){
        List<Account> as=accountDao.listAllAccount();
        for (Account a : as) {
            System.out.println(a);
            System.out.println(a.getUser());
        }

    }

    @Test
    public void listAllAccountUser(){
        List<AccountUser> aus=accountDao.listAllAccountUser();
        for (AccountUser au : aus) {
            System.out.println(au);

        }
    }



}
