package com.zgf.dao;

import com.zgf.domain.Account;
import com.zgf.domain.AccountUser;

import java.util.List;

/**
 * @author zgf
 * @date 2019/9/4 20:24
 */
public interface AccountDao {

    List<AccountUser>  listAllAccountUser();
    List<Account> listAllAccount();
}
