package com.zgf.dao;

import com.zgf.domain.QueryVo;
import com.zgf.domain.User;

import java.util.List;

public interface UserDao {


    List<User> listAll();



    List<User> findByName(String username);



    List<User> findById(int id);

    List<User> findByVo(QueryVo queryVo);

    List<User> findIfByName(User user);
}
