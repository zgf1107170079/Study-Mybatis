package com.zgf.dao;

import com.zgf.domain.Role;

import java.util.List;

/**
 * @author zgf
 * @date 2019/9/5 19:44
 */
public interface RoleDao {

    List<Role> listAll();
}
