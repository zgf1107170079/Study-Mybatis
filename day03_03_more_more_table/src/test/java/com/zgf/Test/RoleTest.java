package com.zgf.Test;

import com.zgf.dao.RoleDao;
import com.zgf.dao.UserDao;
import com.zgf.domain.Role;
import com.zgf.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author zgf
 * @date 2019/9/5 19:48
 */
public class RoleTest {

    private InputStream in;
    private SqlSession session;
    private RoleDao roleDao;

    @Before
    public void init() throws IOException {
        in = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(in);
        session = sqlSessionFactory.openSession();
        roleDao = session.getMapper(RoleDao.class);
    }

    @After
    public void destroy() throws IOException {
        session.commit();
        session.close();
        in.close();
    }

    /**
     * 遍历
     */
    @Test
    public void listAll() {
        List<Role> rs = roleDao.listAll();
        for (Role r : rs) {
            System.out.println("输出信息：");
            System.out.println(r);
            System.out.println(r.getUsers());
        }
    }
}