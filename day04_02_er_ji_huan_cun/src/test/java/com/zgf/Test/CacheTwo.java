package com.zgf.Test;

import com.zgf.dao.UserDao;
import com.zgf.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class CacheTwo {

    private InputStream in;
    SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws IOException {
        in = Resources.getResourceAsStream("SqlMapConfig.xml");
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(in);
    }
    @After
    public void destroy() throws IOException {

        in.close();
    }


    @Test
    public void findById(){
        SqlSession sqlSession1=sqlSessionFactory.openSession();
        UserDao userDao1=sqlSession1.getMapper(UserDao.class);
        User user1=userDao1.findById(1);
        System.out.println(user1);




        SqlSession sqlSession2=sqlSessionFactory.openSession();
        UserDao userDao2=sqlSession2.getMapper(UserDao.class);
        User user2=userDao2.findById(1);
        System.out.println(user2);


        System.out.println(user1==user2);
    }



}
