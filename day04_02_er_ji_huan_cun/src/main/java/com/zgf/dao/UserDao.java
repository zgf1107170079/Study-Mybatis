package com.zgf.dao;

import com.zgf.domain.User;

import java.util.List;

public interface UserDao {


    List<User> listAll();

    User findById(int id);



}
